package net.techu;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.techu.data.ProductoMongo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosV5Controller {

    private ArrayList<Producto> listaProductos;

    @GetMapping("/v5/productos")
    public ResponseEntity<ArrayList<Producto>> getListaProductos() {
        return new ResponseEntity<ArrayList<Producto>>(listaProductos, HttpStatus.OK);
    }

    @PostMapping("/v5/productos")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto")
    public ResponseEntity<Producto> addProducto(@ApiParam(name="producto",
            type="Producto",
            value="producto a crear",
            example="Producto 1",
            required = true) @RequestBody Producto productoNuevo)
    {
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.CREATED);
    }
}
