package net.techu;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.bbva.Calculadora;

@RestController
public class SaludoController {

    @RequestMapping("/saludo")
    public String index() {
        Calculadora calculadora = new Calculadora();
        double resultado = calculadora.obtenerPrecioFina(100);
        return "Hola, yo soy tu API y el precio final de 100 es " + String.valueOf(resultado);
    }
}
