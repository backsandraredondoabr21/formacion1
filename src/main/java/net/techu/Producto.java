package net.techu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

@ApiModel
public class Producto {
    private int id;
    private String nombre;
    private double precio;

    public Producto(int id,
                    @ApiParam(name="nombre",
                    type="String",
                    value="Nombre del producto",
                    example = "Producto 1",
                    required=true)
                            String nombre,
                    double precio) {
        this.setId(id);
        this.setNombre(nombre);
        this.setPrecio(precio);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
