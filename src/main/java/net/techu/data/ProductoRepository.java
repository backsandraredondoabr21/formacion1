package net.techu.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface ProductoRepository extends MongoRepository<ProductoMongo, String> {

    //private List<ProductoMongo> dataList = new ArrayList<ProductoMongo>();
    @Query("{'nombre':?0}")
    public List<ProductoMongo> findByNombre(String nombre);

    @Query("{'precio': {$gt: ?0, $lt: ?1}}")
    public List<ProductoMongo> findByPrecio(double minimo, double maximo);



}
