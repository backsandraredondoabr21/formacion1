package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductosV4Controller {

    private ArrayList<ProductoMongo> listaProductos = null;
    @Autowired
    private ProductoRepository repository;

    /* Get producto by id */
    @GetMapping(value = "/v4/productosbyid/{id}", produces = "application/json")
    public ResponseEntity<Optional<ProductoMongo>> obtenerProductoPorId(@PathVariable String id)
    {
        Optional<ProductoMongo> resultado = repository.findById(id);
        return new ResponseEntity<Optional<ProductoMongo>>(resultado, HttpStatus.OK);
    }


    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductosPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        List<ProductoMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /* Get lista de productos */

    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v4/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo) {

        System.out.println("Estoy en añadir ProductosV4Controller");
        ProductoMongo resultado=repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }


    // PUT Modificar producto
    @PutMapping("v4/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo) {
        System.out.println("Estoy en actualizando ProductosV4Controller");

        Optional<ProductoMongo> resultado=repository.findById(id);
        if(resultado.isPresent()){
            resultado.get().nombre=productoMongo.nombre;
            resultado.get().precio=productoMongo.precio;
            resultado.get().color=productoMongo.color;
                    }
        ProductoMongo guardado=repository.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    //Delete Eliminar Producto
    @DeleteMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        /* Posibilidad de buscar el producto, después eliminarlo si existe */
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}